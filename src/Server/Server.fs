module Server

open System
open System.IO
open System.Threading.Tasks

open Microsoft.AspNetCore.Http

open FSharp.Control.Tasks.V2
open Giraffe
open Shared

open Microsoft.Azure.WebJobs
open Microsoft.Azure.WebJobs.Extensions.Http
open Microsoft.Extensions.Logging

open Fable.Remoting.Server
open Fable.Remoting.Giraffe

let getInitCounter () : Task<Counter> = task { return { Value = 42 } }

let counterApi = {
    initialCounter = getInitCounter >> Async.AwaitTask
}

let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue counterApi
    |> Remoting.buildHttpHandler

let dummyTaskOption = Unchecked.defaultof<HttpContext> |> Task.FromResult |> Some // Giraffe does not use ctx in task as captured already in middleware invoke method

let final (ctx : HttpContext) = 
  ctx.Response.Body.Flush() //workaround https://github.com/giraffe-fsharp/Giraffe.AzureFunctions/issues/1
  dummyTaskOption

// code from https://github.com/giraffe-fsharp/Giraffe.AzureFunctions
[<FunctionName "Giraffe">]
let run ([<HttpTrigger (AuthorizationLevel.Anonymous, Route = "{*any}")>] req : HttpRequest, context : ExecutionContext, log : ILogger) =
  let hostingEnvironment = req.HttpContext.GetHostingEnvironment()
  hostingEnvironment.ContentRootPath <- context.FunctionAppDirectory
  webApp final req.HttpContext :> Task